k=[];
T=[];
T0=[];
MaxQ=[];
id=[];
fmincon_info=[{}];

ilosc_danych_aproks = length(aproksymacja(1,:));
fprintf('\n\n')
textprogressbar('Aproksymacja FOPDT: ',ilosc_danych_aproks);
for i=1:length(aproksymacja(1,:))
    textprogressbar(i,ilosc_danych_aproks);
    id=[id i];
    szacowane_k=max(aproksymacja(:,i));
    MaxQ=[MaxQ szacowane_k];
    fun = @(x)dopasowanie_inercji(x,aproksymacja(:,i));
    x0 = [szacowane_k, 1, 0];
    A = [];
    b = [];
    Aeq = [];
    beq = [];
    lb = [0,0,0];
    ub = [];
    %aproksymacja FOPDT
    options = optimset('Display', 'off');  %wy��czenie komunikatu w konsoli
    [bestx,fval,exitflag,output] = fmincon(fun,x0,A,b,Aeq,beq,lb,ub,[],options);
    if exitflag == 1 || exitflag == 2
        tekst=['First-order optimality FOPDT dla MaxQ=' num2str(MaxQ(i),'%.3f') ' wynosi:'  num2str(output.firstorderopt) ', sse = ' num2str(fval,'%.2f')];
        fmincon_info=[fmincon_info; {tekst}];
    elseif exitflag == 0
        tekst=[fmincon_info;['0.   Dla aproksymacji MaxQ= ' num2str(MaxQ(i),'%.3f') ' przekroczono maksymaln� liczb� iteracji fmincon']];
        fmincon_info=[fmincon_info; {tekst}];
    elseif exitflag == -1 || exitflag == -2 || exirflag == -3
        tekst=[fmincon_info;['-1,-2,-3   Dla aproksymacji MaxQ= ' num2str(MaxQ(i),'%.3f') ' nie uda�o znale�� si� aproksymacji przez fmincon']];
        fmincon_info=[fmincon_info; {tekst}];
    end
    %tworzenie modelu w dziedzinie s
    k=[k bestx(1)];
    T=[T bestx(2)];
    T0=[T0 bestx(3)];
    s=tf("s");
    K=(k(i)/(s*T(i)+1))*exp(-s*T0(i));
    tt=[0:length(aproksymacja(:,i))-1];
    [y,t,x]=step(K,tt);
    %naniesienie inercji na wykres u�rednionych przebiegow
    figure(5)
    plot(t,y,'LineWidth',2);
    hold on;
    plot(aproksymacja(:,i),'LineWidth',2)
    grid on;
end
textprogressbar(' zakonczono',ilosc_danych_aproks);
disp(fmincon_info);
%macierz z modelami:
%|MaxQ|
%| k  |
%| T  |
%| T0 |

modele=[MaxQ;k;T;T0];
tabela=table(id',MaxQ',k',T',T0','VariableNames',{'Id','stan ust','k','T','T0'});
tabela=sortrows(tabela,2,'descend');
tabela.Id=id';
fprintf('\n\n')
disp('Wyniki aprokstymacji FOPDT:')
disp(tabela);

figure(6)
%plot(tabela.k,'LineWidth',1.5)
hold on
plot(tabela.T,'LineWidth',1.5)
plot(tabela.T0,'LineWidth',1.5)
grid on
xlabel('Id stanu ustalonego');
ylabel('Wartosc');
title('Por�wnanie parametrow modeli FOPDT dla uzyskanych modeli');
legend('T','T0','FontSize',12);
