clear all
close all
clc

%rozmiar macierzy
x=4;
y=4;
%inicjalizacja macierzy
mac=[{}];
%losowanie celu
%przykladowa macierz i numeracja
%| 1 | 2 | 3 |
%| 4 | 5 | 6 |
ile_pol=x*y;
celx=randi([1,x],1,1);
cely=randi([1,y],1,1);
%cel=1;

%inicjalizacja zmiennych
stop=0;
pozx=randi([1,x],1,1);
pozy=randi([1,y],1,1);
mac(cely,celx)={100};

%rozmiar Q-table
stany=y*x;
akcje=4;
%tworzenie Q-table
Q_table=zeros(stany,akcje);
%losowanie wsp a
a = randi([0, 100], [1, 1])/100;
%greedy factor
eps=0.1;

while stop==0
    for i=1:y
        for j=1:x
%             if i==cely && j==celx
%                 mac(i,j)={100};
            if i==pozy && j==pozx
                mac(i,j)={'x'};
            else
                mac(i,j)={' '};
            end
        end
    end
    mac
    prompt=' test';
    %     wybor = input(prompt)
    w = waitforbuttonpress;
    if w
        wybor = get(gcf, 'CurrentCharacter');
        %        disp(p) %displays the character that was pressed
                 disp(double(wybor))  %displays the ascii value of the character that was pressed
    end
    %strzalka gora
    if double(wybor)==30
        if pozy-1>=1
            pozy=pozy-1;
        end
    end
    %strzalka w prawo
    if double(wybor)==29
        if pozx+1<=x
            pozx=pozx+1;
        end
    end
    %strzalka w dol
    if double(wybor)==31
        if pozy+1<=y
            pozy=pozy+1;
        end
    end
    %strzalka w lewo
    if double(wybor)==28
        if pozx-1>=1
            pozx=pozx-1;
        end
    end
    %esc
    if double(wybor)==27
        stop=1;
        disp('Zamknieto program')
        close all
    end
    
    %warunek stopu
    if pozx == celx && pozy==cely
        disp('Zdobyto nagrode');
        close all
        stop=1;
    end
    
end
