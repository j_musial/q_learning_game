clear all
close all
clc
alfa=0.1;
gamma=0.9;
R=100;
q=[];
for i=1:20
    q(1,i)=0;
end
for i=2:2000
    for wart=20:-1:1
        if wart==1
            R=100;
        else
            R=0;
        end
        if wart==1
            q(i,wart)=q(i-1,wart)+alfa*(R+gamma*q(i-1,wart+1)-q(i-1,wart));
        elseif wart == 20
            q(i,wart)=q(i-1,wart)+alfa*(R+gamma*q(i-1,wart-1)-q(i-1,wart));
        else
            q(i,wart)=q(i-1,wart)+alfa*(R+gamma*max(q(i-1,wart-1),q(i-1,wart+1))-q(i-1,wart));
        end
    end
end
    plot(q);
    hold on;
 
    xlabel('iteracje');
    ylabel('value');
close all
