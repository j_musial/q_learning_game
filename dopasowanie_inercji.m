function [sse] = dopasowanie_inercji(x,dane)
k=x(1);
T=x(2);
if x(3)<0
    x(3)=0;
end
T0=x(3);

s=tf("s");
K=(k/(s*T+1))*exp(-s*T0);
tt=[0:length(dane)-1];
[y,t,x]=step(K,tt);

sse=sum((dane - y).^2);
end

