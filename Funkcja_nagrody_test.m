clear all
close all
clc

x=4;
y=4;
mac=zeros(x,y);
meta=[x,1];
for i=1:x
    for j=1:y
        mac(i,j)=sqrt((meta(1)-i)^2+(meta(2)-j)^2);
    end
end
mac