function [Kr,Ti,Td] = nastawy_regulator(fun, metoda, regul,czy_rys)

if length(cell2mat(fun.Denominator)) == 2
    ijdsadsaoijdsa
    Tob=cell2mat(fun.Denominator);
    Tob=Tob(1);
    Kob=cell2mat(fun.Numerator);
    Kob=Kob(2);
    Kr=Tob/(0.8*Tob*Kob)
    Ti=Tob
    
else
    
    %FOPDT
    [y,t]=step(fun);
    ymax=max(y);
    ymin=min(y);
    
    p63=0.632*ymax;
    p28=0.283*ymax;
    
    flaga1=0;
    flaga2=0;
    
    for i=1:length(y)
        if y(i) >= p28 && flaga1==0
            t2=t(i);
            flaga1=1;
        end
        if y(i) >= p63 && flaga2==0
            t1 = t(i);
            flaga2=1;
        end
    end
    
    k=y(end)-y(1);
    T=1.5*(t1-t2);
    T0=t1-T;
    
    if czy_rys==1
        fop_2pkt=tf([k],[T 1], 'InputDelay',T0);
        
        figure
        step(fun);
        hold on;
        step(fop_2pkt);
        grid on;
        legend('fun','metoda 2 pkt')
        title('Por�wnanie fopdt do funkcji oryginalnej');
    end
    flaga_blad=0;
    %PI
    if regul=='PI'
        Td=0;
        
        if length(metoda)==3
            if metoda == 'chr'
                Kr=0.6*(T/(k*T0));
                Ti=4*T0;
            elseif metoda == 'qdr'
                Ti=0.33*T0;
                Kr=0.9*(T/(k*T0));
            else
                flaga_blad=1;
            end
        elseif length(metoda)==2
            if metoda == 'zn'
                Ti = T0/0.3;
                Kr = (0.9*T)/(k*T0);
            elseif metoda == 'cc'
                alfa=T0/T;
                Ti = T0*((30+3*alfa)/(9+20*alfa));
                Kr = (T/(k*T0))*(0.9 + alfa/12);
            else
                flaga_blad=1;
            end
        else
            flaga_blad=1;
        end
    elseif regul == 'PID'
        %NASTAWY PID;
    else
        flaga_blad=1;
    end
    
    if flaga_blad==1
        disp("Z�y typ regulatora")
        Ti=inf;
        Kr=inf;
        Td=inf;
    end
end

end