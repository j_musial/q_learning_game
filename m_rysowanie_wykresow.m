%wykres 3d z plansza gry
figure(1)
wsk=1-0.15*lpo;
s=surf(TT,'FaceAlpha',1);
hold on;
xlabel('x');
ylabel('y');
zlabel('Max Q(s,:)');
title('Wagi na planszy gry');

%ilosc ruchow danej epoce
figure(2)
plot(epoka_wek,dane_wek);
hold on;
grid on;
xlabel('epoka');
ylabel('ilosc ruchow');

%wypisanie Q_table
% fprintf('\n');
% disp('         y         x       gora      dol      lewo      prawo       stop');
% disp(cell2mat(compose('%10.0f',Q_table)))

%wyswietlenie planszy z naniesionymi wagami
fprintf('\n');
disp('Plansza z naniesionymi wagami:')
disp(cell2mat(compose('%10.0f',TT)))

%dynamika nauczania dla poszcegolnych stanow
close all
figure(3)
for i=1:x
    for j=1:y
        plot(squeeze(q_3d(i,j,:)));
        hold on;
    end
end
mnoznik=ylim;
mnoznik=mnoznik(2);
limit=length([0 norm_wek_warunek]);
%plot([1:length(squeeze(q_3d(1,1,:)))],[0 norm_wek_warunek*mnoznik]);
h = area([0 norm_wek_warunek*mnoznik],'LineStyle',':');
h.FaceAlpha = 0.3;
xlabel('epoki');
ylabel('Max Q(s,:)');
nap=[{'Warto�ci maksymalnych akcji w stanach}, {obszar-spelniona norma ' num2str(dokl_norm)}];
title(nap);
grid on;
xlim([0,limit])

%wyswietlanie zmiennosci normy w czasie
figure(4)
yyaxis left;
plot(norm_wek);
xlabel('epoki');
ylabel('norm [sum(Q_i - Q_{i-1})]');
title('Zmiana normalizacji Max Q(s,:) w kolejnych epokach');
grid on;
yyaxis right
plot(norm_wek_warunek);
ylabel('Spe�nienie dokladnosci normalizacji');
ylim([0,2]);


%u�rednione przebiegi dla danych stanow ustalonych
figure(5)
plot(aproksymacja,'LineWidth',1.5)
grid on
xlabel('epoki');
ylabel('�rednie Max Q(s,:)');
title('Aproksymowane przebiegi dla danych stan�w ustalonych');



