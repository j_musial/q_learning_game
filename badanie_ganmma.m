clc
close all
clear all

wyk=1;
%wsp_gamma=[0.1 0.5 1 10];

for wsp_gamma=[0.1 0.5 1 10]
    wsp_gamma
    i=0;j=1;
    
    for n=2:1:1000
        i=i+1;
        j=1;
        for R=1:1:1000
            gamma(i,j)=(wsp_gamma/R)^(1/(n-1));
            Qn=gamma(i,j)^n*R/(1-gamma(i,j));
            Qn1=gamma(i,j)^(n-1)*R/(1-gamma(i,j));
            roznica=Qn-Qn1;
            mac(i,j)=roznica;
            j=j+1;
        end
    end
    figure(wyk)
    mesh(gamma);
    hold on;
    xlabel('R');
    ylabel('n');
    zlabel('gamma');
    wyk=wyk+1;
    grid on;
    title(['Wartosc gamma dla wsp = ',num2str(wsp_gamma)]);
    
    figure(wyk+4)
    mesh(mac);
    hold on;
    xlabel('R');
    ylabel('n');
    zlabel('gamma');
    wyk=wyk+1;
    grid on;
    title(['Wartosc roznicy dla wsp = ',num2str(wsp_gamma)]);
    
    
    %title('Badanie gamma');
    
    % xticks([0 10 20 30 40 50 60 70 80 90 100])
    % xticklabels({'0', '100', '200','300','400','500','600','700','800','900','1000'})
    % yticks([0 10 20 30 40 50 60 70 80 90 100])
    % yticklabels({'0', '100', '200','300','400','500','600','700','800','900','1000'})
    
    %     R=100;
    %     n=300;
    %
    %     for wsp_gamma=0:0.01:10
    %         gamma2(i)=(wsp_gamma/R)^(1/(n-1));
    %     end
    % figure(wyk)
    % plot(gamma2)
    
end