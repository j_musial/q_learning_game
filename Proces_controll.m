clear all
close all
clc

%Dane
u0=20;
dist=0;

%load('ini.mat')

%obiekt
s = tf('s');
plant = 1/(1+5*s);
%opoznienie
T0=0;

[Kr,Ti,Td] = nastawy_regulator(plant, 'chr', 'PI',0);

%ilosc stanow
ile_st = 20;
szer_st=0.5;

%ilosc akcji bez koncowej
ile_ac = 12;

Q_table=zeros(ile_st+1,ile_ac);

state_table(1)=-ile_st/2*szer_st;
for i=2:ile_st
    if state_table(i-1)+szer_st == 0
   state_table(i)=state_table(i-1)+szer_st+szer_st;
   i=i+1;
    else
        state_table(i)=state_table(i-1)+szer_st;
    end
end
state_table
action_table=[-5 -2 -1 -0.5 -0.25 -0.5 0.5 0.25 0.5 1 2 5];
num_of_actions=length(action_table);

kQ=3;
alfa=0.7;
gamma=0.95;
eps=0;
  options = simset('SrcWorkspace','current');
  sim('Q_sim_3.slx',[],options);


