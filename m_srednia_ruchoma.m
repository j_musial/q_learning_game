
close all;


okno=15;
sr_wek=[];
x2=[];
for i=1:length(norm_wek)
    if i>okno/2+1 && i<length(norm_wek)-okno/2-1
   sr=mean(norm_wek(i-okno/2:i+okno/2));
    else
        sr=0;
    end
   sr_wek=[sr_wek sr];
   x2=[x2 i];
end

x1=0:1:length(norm_wek);

%wyswietlanie zmiennosci normy w czasie
figure(4)
plot(norm_wek);
xlabel('epoki');
ylabel('norm [sum(Q_i - Q_{i-1})]');
title('Zmiana normalizacji Max Q(s,:) w kolejnych epokach');
grid on;
hold on;
%plot(sr_wek,'LineWidth',2);
ylim([0 1]);