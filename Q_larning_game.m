clear all
close all
clc

%rozmiar macierzy
x=8;
y=8;
%inicjalizacja macierzy
mac=[{}];
dane=[];
dane_iter=[];
%losowanie celu
%przykladowa macierz i numeracja
%| 1 | 2 | 3 |
%| 4 | 5 | 6 |
ile_pol=x*y;
celx=randi([1,x],1,1);
cely=randi([1,y],1,1);
%cel=1;

%rozmiar Q-table
stany=y*x;
akcje=4;
%tworzenie Q-table
Q_table=zeros(stany+1,akcje+2);

%uzupe�nianie stanow i akcji (nag�owki tabeli q)
Q_table(1,1)=NaN;
Q_table(1,2)=NaN;
%numery akcji
for i=1:akcje
    Q_table(1,i+2)=i;
end
%numery stanow x i y
m=0;
for i=1:y
    for j=1:x
        Q_table(j+1+m,1)=i;
        Q_table(j+1+m,2)=j;
    end
    m=y*i;
end

%kszta� Q-table
%stan/akcje
%| NaN | NaN |gora | dol |lewo |prawo|
%|  y1  | x1 |  Q  |  Q  |  Q  |  Q  |
%|  y1  | x2 | ... | ... | ... | ... |
Q_table

%greedy factor
eps=0;
%learning rate
alfa=0.2;
%discount rate
gamma=0.9;

powtorzenia=0;

%% iteracje
kroczek=0.02;
for epoka=1:(1/kroczek)
    
    eps=eps+kroczek

    %fprintf('\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n');
    fprintf('Epoka = %2.0f, powtorzenia = %3.0f, eps = %2.2f\n',epoka,powtorzenia,eps);
    pause(0.2);
    %inicjalizacja zmiennych
    stop=0;
    pozx=randi([1,x],1,1);
    pozy=randi([1,y],1,1);
    mac(cely,celx)={100};
    
    
    powtorzenia=0;
    while stop==0
        powtorzenia=powtorzenia+1;
        for i=1:y
            for j=1:x
                %             if i==cely && j==celx
                %                 mac(i,j)={100};
                if i==pozy && j==pozx
                    mac(i,j)={'x'};
                elseif i==cely && j==celx
                    mac(i,j)={'O'};
                else
                    mac(i,j)={' '};
                end
            end
        end
        %drukowanie na ekranie
        fprintf('\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n');
        eps
        mac
        %% logika
        
        %losowanie wsp a
        a = randi([0, 100], [1, 1])/100;
        
        %Szukanie rekordu w Q_table
        for i=2:stany+1
            if Q_table(i,1)==pozy && Q_table(i,2)==pozx
                wek_ruch=[Q_table(i,3:6)];
                wybrany_stan=i;
                break;
            end
        end
        %exploration/exploitation problem
        if eps<=a
            wybor=randi([1, 4], [1, 1]);
            disp('losowo');
        else
            disp('tabela');
            [Qsa, wybor]=max(wek_ruch);
            ile_powt=sum(wek_ruch(:)==wybor);
            if ile_powt>1
                los=randi([1, ile_powt], [1, 1]);
                iter=1;
                for i=1:4
                    if wek_ruch(i) == Qsa && iter == i
                        wybor=i;
                    end
                    iter=iter+1;
                end
            end
        end
        pozx_old=pozx;
        pozy_old=pozy;
        %% ruch gracza
        %strzalka gora
        if double(wybor)==1
            if pozy-1>=1
                pozy=pozy-1;
            end
        end
        %strzalka w dol
        if double(wybor)==2
            if pozy+1<=y
                pozy=pozy+1;
            end
        end
        %strzalka w lewo
        if double(wybor)==3
            if pozx-1>=1
                pozx=pozx-1;
            end
        end
        %strzalka w prawo
        if double(wybor)==4
            if pozx+1<=x
                pozx=pozx+1;
            end
        end
        %esc
        if double(wybor)==27
            stop=1;
            disp('Zamknieto program')
            close all
        end
        %warunek stopu
        if pozx == celx && pozy==cely
            fprintf('\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n');
            disp('Zdobyto nagrode');
            close all
            stop=1;
        end
        
        %% upgrade q-table
        
        %warto�� nagrody (odleg�o�� od celu)
        if pozx_old==pozx && pozy_old==pozy
            R=-25;
            Q_table(wybrany_stan,wybor+2)=Q_table(wybrany_stan,wybor+2) + alfa*(R);
        else
            R=(5-sqrt((celx-pozx)^2+(cely-pozy)^2))^3
            if R==25
                R=R^3;
            end
            %max Q(St+1, a)
            for i=2:stany+1
                if Q_table(i,1)==pozy && Q_table(i,2)==pozx
                    maxS=max(Q_table(i,3:6));
                    break;
                end
            end
            
            %poprawa warto�ci w Q-table
            Q_table(wybrany_stan,wybor+2)=Q_table(wybrany_stan,wybor+2) + ...
                alfa*(R+gamma*maxS-Q_table(wybrany_stan,wybor+2));
            %def wikipedi67
            %Q_table(wybrany_stan,wybor+2)=(1-alfa)*Q_table(wybrany_stan,wybor+2)+...
              %  alfa*(R+gamma*maxS);
            
        end
        
        
        %zmodyfikowany wzor bez normalizacji
        %Q_table(wybrany_stan,wybor+2)=Q_table(wybrany_stan,wybor+2) + alfa*(R);
        if stop==0
        %Q_table
        end
        pause(0.1);
    end
    dane=[dane, powtorzenia];
end
plot(dane)
