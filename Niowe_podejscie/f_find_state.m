function stan = f_find_state(e, table)

%Funkcja znajduje aktualny numer stany w ktorym znajduje sie regulator na
%podstawie wartosci uchybu

stan=0;
if e>=table(length(table))
    stan=length(table)+1;
elseif e < table(1)
    stan=1;
else
    for i=1:length(table)
        if e>=table(i)
            stan=i+1;
        end
    end
end
end