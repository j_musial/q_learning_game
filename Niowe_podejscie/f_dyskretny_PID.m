function [u,calka] = f_dyskretny_PID(typ,e,Kp,Ti,Td,Tn,dt,calka,e_prev)
%funkcja realizująca dyskretny regulator PID, string w polu pid musi byc
%czteroznakowy by dzialalo. Jezeli uzywany jest PID to w pierwszej iteracji
%wykorzystac PI poniewaz nie ma jeszcze e_prev

if typ=='P   '
    u=Kp*e;
end

%reg_PI
if typ=='PI  '
    calka=calka+e;
    u=Kp*(e+(1/Ti)*calka*dt);
end

if typ=='PID '
    calka=calka+e;
    u=Kp*(e+(1/Ti)*calka*dt+Td*((e-e_prev)/dt));
end

if typ=='I   '
    calka=calka+e;
    u=(1/Ti)*calka*dt;
end
%niedokonczone bo chyba nie bedzie uzywane
% if typ=='PIDn'
%     calka=calka+e;
%     u=Kp*(e+(1/Ti)*calka*dt+Td*((e-e_prev)/td));
% end

end

