

%konfiguracja

clear logi2
% close all
%inicjalizacja danych, wst�pne obliczenia

[plant,wyb_obiekt]=f_obiekt_inicjalizacja(1,k,T(1));
[Kp,Ti,Td,wyb_nastawy, wyb_reg,T_fopt,k_fopt,T0_fopt] = nastawy_regulator(plant, 'chr', 'PI',0,dt);

%% WEKTORY %%
y=[round((SP-granica-granica*0.1)*100)/100];
y_PID=[y];
e=[];
u=[];
t=[];
e_PID=[];
u_PID=[];

z=0;
norma=[];

% y_PID=[0]

%% ZMIENNE POMOCNICZE %%
iter=1;
info_num=0;
logi_num=1;
czas_stabil=0;
epoka=0;
norma_iter=0;
norma_start=0;
i=0;

i=0;
iter=1;
eps=1;

%% petla regulacji
for i=1:3000
    
    
    
    if act_state == ile_st/2+1
        czas_stabil=czas_stabil+dt;
        if czas_stabil > T_fopt*20
            break;
        end
        
    else
        czas_stabil=0;
    end
    
    %wektor czasu
    if i == 1
        t=[0];
    else
        t=[t t(end)+dt];
    end
    
    %obliczanie e(i)
    %     if iter~=1
    %         e(iter)=SP-y(iter);
    %         e_PID(iter)=SP-y_PID(iter);
    %     else
    %         e(iter)=SP;
    %         e_PID(iter)=SP;
    %     end
    
    e(iter)=SP-y(iter);
    e_PID(iter)=SP-y_PID(iter);
    
    %Q-learning
    
    %losowanie wsp a
    a = randi([0, 100], [1, 1])/100;
    
    act_state=f_find_state(e(iter),state_table);
    
    %badanie czy osi�gnieto stan docelowy
    if (act_state == ile_st/2+1) && (iter ~= 1)
        wyb_akcja=ile_ac+1;    % <-- osiagnieto, nie zmieniaj sterowania
        
        %nagroda
        R=nagroda;
        
    else
        
        %nagroda
        %         R=0;
        
        %jezeli najdalsze stany to skrajne akcje
        if act_state==ile_st+1  && 0
            wyb_akcja=ile_ac;
            % wyb_akcja=1;
        elseif act_state==1  && 0
            wyb_akcja=1;
            %wyb_akcja=ile_ac;
            %nie osiagnieto stanu docelowego
            
            %exlotation/exploration dilema
        else
            if eps<=a
                wyb_akcja=randi([1, ile_ac+1], [1, 1]);
            else
                [wart_akcji,wyb_akcja]=f_best_action_in_state(Q_table,act_state);
            end
        end
    end
    %obliczanie u
    if iter>1
        u(iter)= (kQ*action_table(wyb_akcja)) + u(iter-1);
    else
        u(iter)= (kQ*action_table(wyb_akcja));
        u(iter)=y(1)/k;
        calka=(Ti*(u(iter)-Kp*e_PID(iter))/(Kp*dt))   %inicjowanie calki w PI
        calka=(u(iter)*Ti)/dt;
    end
    
    
    
    %obliczanie u(i)
    [u_PID(iter),calka] = f_dyskretny_PID('I   ',e_PID(iter),Kp,Ti,Td,Tn,dt,calka,0);
    
    %ograniczenia
    if u(iter)<0
        u(iter)=0;
    elseif u_PID(iter)<0
        u_PID(iter)=0;
    end
    
     if u(iter)>100
        u(iter)=100;

    elseif u_PID(iter)>100
        u_PID(iter)=100;  
    end
    
    %dodanie szumu
    if dist_on==1
        z=randi(100,1)/2000;
    end
    
%     for pet_wew=1:5
%         %obliczanie y(i+1) dla PID
%         y_PID(iter+1)=f_obiekt(1,dt/5,k,T,y_PID(iter),u_PID(iter))+z;
%         
%         %obliczanie y(i+1) dla Q_learning
%         y(iter+1)=f_obiekt(1,dt/5,k,T,y(iter),u(iter))+z;
%     end

    for pet_wew=1:1
        %obliczanie y(i+1) dla PID
        y_PID(iter+1)=f_obiekt(1,dt,k,T,y_PID(iter),u_PID(iter))+z;
        
        %obliczanie y(i+1) dla Q_learning
        y(iter+1)=f_obiekt(1,dt,k,T,y(iter),u(iter))+z;
    end

    e_s=SP-y(iter+1);
    old_state=act_state;
    act_state=f_find_state(e_s,state_table);
    
    maxS=f_find_maxS(Q_table, act_state, ile_st+1,wyb_akcja);
    
    
    logi2(logi_num).maxS=maxS;
    logi2(logi_num).act_state=act_state;
    logi2(logi_num).wyb_akcja=wyb_akcja;
    
    %inkremantacja licznika i akwizycja
    iter=iter+1;
    logi_num=logi_num+1;
    %wyswietlanie statusu w konsoli
    
    
    
end

%Q_table

figure('Position',[ (rozm_ekranu(3)/3)*2 rozm_ekranu(4)/2 rozm_ekranu(3)/3 rozm_ekranu(4)/2])
plot(t,u);
grid on;
hold on;
plot(t,e);
plot(t,y(1:end-1));
legend('u','e','y');
title('Porownanie wartosci sygnalow w Q_learning')


figure('Position',[rozm_ekranu(3)/2 1 rozm_ekranu(3)/2 rozm_ekranu(4)/2])
plot(t,y(1:end-1))
hold on
plot(t,y_PID(1:end-1))
grid on;
legend('Q_learning','PID');
title('Porownanie sygfna�y wyj�cuiowego dla Q-learning i PID')

figure('Position',[1 1 rozm_ekranu(3)/2 rozm_ekranu(4)/2])
subplot(3,1,1)
plot(t,[logi2.act_state]);
title('Wykryte stany');
grid on;
yline(ile_st/2+1,'r')

subplot(3,1,2)
plot(t,[logi2.wyb_akcja]);
grid on;
title('Wybrane numery akcji');
yline(ile_ac+1,'r')

subplot(3,1,3)
plot(t,[logi2.maxS]);
grid on
title('Wartosci maxS');


figure(1)
figure(2)
figure(5)


% czas_koniec=t(end)
% sim('test_pidy')
% 
% figure()
% plot(y_PID(1:end-1));
% hold on
% plot(y_sim')
% legend('iteracja','sym')







