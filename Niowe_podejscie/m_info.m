%plik generujacy informacje wyswietlane w konsoli podczas symulacji

info_num=info_num+1;

fileID = fopen('dane.txt','w');

switch info_num
    case 1
        fprintf('Q_learning_simulation %s\nDane wej�ciowe:\n',datestr(datetime,0));
        fprintf('Wybrany model:\n%s\n\n',wyb_obiekt);
        fprintf('Parametry PID:\nTyp: %s, nastawy: %s  Kr = %.2f, Ti = %.2f, Td = %.2f\n\n',wyb_reg,wyb_nastawy,Kp,Ti,Td);
        fprintf('Parametry Q-learning:\nAlfa = %.2f, gamma = %.2f, wsp. gamma = %.2f,nagroda = %d\n',alfa,gamma,wsp_gamma,nagroda);
        fprintf('SP = %.2f, kQ = %.2f, zaklucenie =%d\n',SP, kQ, dist_on);
        fprintf('ile_st = %d, szer_st = %.2f, ile_ac = %d, szer_ac = %.2f',ile_st,szer_st,ile_ac,szer_ac);
        fprintf('Ilo�c epok = %d, max. ilo�� ruch�w = %d, dok�adno�� FOPDT = %.2f\n\n',max_epoki,max_iter,dokladnosc_aproksymacji);
        fprintf('Parametry symulacji:\ndt = %f, czas symulacji %.2f\n\n',dt,czas_koniec);
        
        
        
        fprintf(fileID,'Q_learning_simulation %s\nDane wej�ciowe:\n',datestr(datetime,0));
        fprintf(fileID,'Wybrany model:\n%s\n\n',wyb_obiekt);
        fprintf(fileID,'Parametry PID:\nTyp: %s, nastawy: %s  Kr = %.2f, Ti = %.2f, Td = %.2f\n\n',wyb_reg,wyb_nastawy,Kp,Ti,Td);
        fprintf(fileID,'Parametry Q-learning:\nAlfa = %.2f, gamma = %.2f, wsp. gamma = %.2f,nagroda = %d\n',alfa,gamma,wsp_gamma,nagroda);
        fprintf(fileID,'SP = %.2f, kQ = %.2f, zaklucenie =%d\n',SP, kQ, dist_on);
        fprintf(fileID,'ile_st = %d, szer_st = %.2f, ile_ac = %d, szer_ac = %.2f',ile_st,szer_st,ile_ac,szer_ac);
        fprintf(fileID,'Ilo�c epok = %d, max. ilo�� ruch�w = %d, dok�adno�� FOPDT = %.2f\n\n',max_epoki,max_iter,dokladnosc_aproksymacji);
        fprintf(fileID,'Parametry symulacji:\ndt = %f, czas symulacji %.2f\n\n',dt,czas_koniec);
end

fclose(fileID);

