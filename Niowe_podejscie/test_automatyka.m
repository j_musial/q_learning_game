clear all
close all
clc

y=[];
e=[];
u=[];
t=[];

SP=3;

k=2;
T=10;

Kp=2;
Ti=20;
Td=0;

dt=0.01;
y(1)=0;
iter=1;
calka=0;

Tn=0;




for i=0:dt:30
    %wektor czasu
    t=[t i];
    
    %obliczanie e(i)
    if i~=0
        e(iter)=SP-y(iter);
    else
        e(iter)=SP;
    end
    
    %obliczanie u(i)
    if iter==1
        [u(iter),calka] = f_dyskretny_PID('PI  ',e(iter),Kp,Ti,Td,Tn,dt,calka,0);
    else
        [u(iter),calka] = f_dyskretny_PID('PID ',e(iter),Kp,Ti,Td,Tn,dt,calka,e(iter-1));
    end
    
    %obliczanie y(i+1)
    
    y(iter+1)=y(iter)+dt/T*(-y(iter)+k*u(iter));
    
    iter=iter+1;
end

figure()
plot(t,e);
hold on
grid on
plot(t,u);
plot(t,y(1:end-1));
legend('e','u','y');
title('CODE SIMULATION')

sim('test_pidy.slx')


figure()
subplot(3,1,1)
plot(t,u);
hold on;
grid on;
plot(tout,u_sim);
title('Porownanie u(t)');
legend('disc','sim')
subplot(3,1,2)
plot(t,e);
hold on;
grid on;
plot(tout,e_sim);
title('Porownanie e(t)');
subplot(3,1,3)
plot(t,y(1:end-1));
hold on;
grid on;
plot(tout,y_sim);
title('Porownanie y(t)');


% figure()
% plot(t,u);
% hold on;
% plot(t,uu);
% grid on;
% legend('u','uu');
% title('Porownanie pid w funkcji jak i bez');


for i=1:nis
    WU=[u,X1P(1:(alfam-1))];
    X1=X1P+tcTob.*(k*WU-X1P);
    X1P=X1;
end
Wy=X1(alfam);








