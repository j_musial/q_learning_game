clear all
close all
clc

dane=[];

format long
%konfiguracja

%inicjalizacja danych, wst�pne obliczenia
m_inicjalizacja
[plant,wyb_obiekt]=f_obiekt_inicjalizacja(1,k,T(1));
[Kp,Ti,Td,wyb_nastawy, wyb_reg,T_fopt,k_fopt,T0_fopt] = nastawy_regulator(plant, 'chr', 'PI',0,dt);


%ilosc stanow (parzyste)
ile_st = 40;
szer_st=0.2;

%ilosc akcji bez koncowej (parzyste)
ile_ac = 120;
szer_ac=0.5;

y_start=0;

Q_table=zeros(ile_st+1,ile_ac+1);

state_table(1)=-ile_st/2*szer_st;
for i=2:ile_st
    if state_table(i-1)+szer_st > -10^-10 && state_table(i-1)+szer_st < 10^-10
        state_table(i)=state_table(i-1)+szer_st+szer_st;
        i=i+1;
    else
        state_table(i)=state_table(i-1)+szer_st;
    end
end
state_table

for i=1:(ile_ac/2)
    action_table=[action_table szer_ac*i szer_ac*-i];
end
action_table=[sort(action_table) 0]
%action_table=[-5 -2 -1 -0.5 -0.25 -0.5 0.5 0.25 0.5 1 2 5 0]

m_info  %case 1
% upd = pasek(czas_koniec);
i=0;

n=length(state_table)*length(action_table);  
gamma=((wsp_gamma/nagroda))^(1/(n-1));  

%% petla regulacji
while 1
    i=i+1;
    %warunek stopu
    %if (norma_iter == norma_iter_stop && epoka>2) || epoka == max_epoki
    if epoka == max_epoki
        break;
    end
    
    
    if act_state == ile_st/2+1
        czas_stabil=czas_stabil+dt;
        norma_start=1;
        if czas_stabil > T_fopt*3
            norma_iter_stop=1;
            epoka=epoka+1;
            if max_epoki < 5000
                dzielnik=100;
            else
                dzielnik=1000;
            end
            
            if mod(epoka,dzielnik) == 0
                fprintf('\n%d ',epoka);
            end
            
           % dane=[dane [0;0;0]];
%             Q_table
            m_reset
            
        end
        
    else
        czas_stabil=0;
    end
    
    
    stare_Q=Q_table;
    
    %wektor czasu
%     if i == 1
%         t=[0];
%     else
%         t=[t t(end)+dt];
%     end
%     
    %obliczanie e(i)
%     if iter~=1
%         e_s=SP-y_s;
%         % e_PID(iter)=SP-y_PID(iter);
%     else
%         e_s=SP;
%         % e_PID(iter)=SP;
%     end
    
    e_s=SP-y_s;
    %Q-learning
    
    %losowanie wsp a
    a = randi([0, 100], [1, 1])/100;
    %stary_stan=act_state;
    act_state=f_find_state(e_s,state_table);
    
    %badanie czy osi�gnieto stan docelowy
    if (act_state == ile_st/2+1) && (iter ~= 1)
        wyb_akcja=ile_ac+1;    % <-- osiagnieto, nie zmieniaj sterowania
        
        %nagroda
        R=nagroda;
        
    else
        R=0;
        
        %jezeli najdalsze stany to skrajne akcje
        if act_state==ile_st+1
            wyb_akcja=ile_ac;
        elseif act_state==1
            wyb_akcja=1;
            %nie osiagnieto stanu docelowego
            
            %exlotation/exploration dilema
        else
            if eps<=a
                wyb_akcja=randi([1, ile_ac+1], [1, 1]);
            %wek_akcji=[wek_akcji wyb_akcja];
            else
                [wart_akcji,wyb_akcja]=f_best_action_in_state(Q_table,act_state);
                
            end
        end
    end
    %obliczanie u
    if iter>1
        u_s= (kQ*action_table(wyb_akcja)) + u_s;
    else
       % u_s= (kQ*action_table(wyb_akcja));
        u_s=y_start/k;
        %u_s=0;
    end
    
    if u_s<0 
        u_s=0;  
    end
    if u_s>100
        u_s=100;
    end
    
    %obliczanie u(i)
    %[u_PID(iter),calka] = f_dyskretny_PID('PI  ',e_PID(iter),Kp,Ti,Td,Tn,dt,calka,0);
    
    %dodanie szumu
    if dist_on==1
        z=randi(100,1)/2000;
    end
    
    %obliczanie y(i+1) dla PID
    % y_PID(iter+1)=f_obiekt(1,dt,k,T,y_PID(iter),u_PID(iter))+z;
    
%     for pet_wew=1:5
%     %obliczanie y(i+1) dla Q_learning
%     y_s=f_obiekt(1,dt/5,k,T,y_s,u_s)+z;
%     end
    
     for pet_wew=1:1
    %obliczanie y(i+1) dla Q_learning
    y_s=f_obiekt(1,dt,k,T,y_s,u_s)+z;
    end
    
    
   % dane=[dane, [u_s;e_s;y_s]];
    %update Q-table
   
    e_s=SP-y_s;
    old_state=act_state;
    act_state=f_find_state(e_s,state_table);
    
%     if (act_state<ile_st/2+1 && old_state>act_state) || (act_state>ile_st/2+1 && old_state<act_state)
%         R=0;
        
%     if (act_state == ile_st/2+1) && (iter ~= 1)
%         R=nagroda;
%         disp('hue')
%     else
%         R=0;
%         
%     end
    %nagroda
    %         R=-1;
    
    
    %%%%%
    %NORMALIZACJA TABLEI Q PRZEZ DZIELENIE BY WYKRESY WARTOSCI BYLY
    %WYRAZNIEJSZE!!!!!!!!!!!!
    %%%%%%%%%%
    
     maxS=f_find_maxS(Q_table, act_state, ile_st+1,wyb_akcja);
    
%     Q_table(act_state,wyb_akcja)=Q_table(act_state,wyb_akcja) + ...
%         alfa*(R+gamma*maxS-Q_table(act_state,wyb_akcja));
    
    Q_table(old_state,wyb_akcja)=Q_table(old_state,wyb_akcja) + ...
        alfa*(R+gamma*maxS-Q_table(old_state,wyb_akcja));
    
    %norma
    norma=[norma sum(sum(Q_table-stare_Q))];
    
    
    
    if norma(end)<dokl_norm
        norma_iter=norma_iter+1;
    else
        norma_iter=0;
    end
    
    if length(norma)>1010
        norma=norma(end-10:end);
    end
    
    %inkremantacja licznika i akwizycja
   % m_akwizycja_danych
    iter=iter+1;
    %wyswietlanie statusu w konsoli
    
    %     upd(i);
    
    
    
    
end



m_rysuj_wykresy

m_test_nauczonej

figure(3)

% figure()
% plot(dane')
% legend('u','e','y')
% grid on


