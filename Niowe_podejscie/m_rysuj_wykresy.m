

rozm_ekranu=get(0,'ScreenSize');
rozm_ekranu(3)=rozm_ekranu(3)-50;


% figure()
% plot(tt,y(1:end-1))
% hold on
% plot(tt,y_PID(1:end-1))
% grid on;
% legend('Q_learning','PID');
% title('Porownanie sygfna�y wyj�cuiowego dla Q-learning i PID')

% figure()
% subplot(4,1,1)
% plot(t,[logi.act_state]);
% title('Wykryte stany');
% grid on;
%
% subplot(4,1,2)
% plot(t,[logi.wyb_akcja]);
% grid on;
% title('Wybrane numery akcji');
%
% subplot(4,1,3)
%
%


% figure('Position',[1 rozm_ekranu(4)/2 rozm_ekranu(3)/3 rozm_ekranu(4)/2])
% plot(t,[logi.maxS]);
% grid on
% title('Wartosci maxS');



%
% subplot(4,1,4)
% plot(t,[logi.przyrost]);
% grid on;
% title('Wylosowane wartosci przyrostu');

% figure()
% plot(tt,u);
% grid on;
% hold on;
% plot(tt,e);
% plot(tt,y(1:end-1));
% legend('u','e','y');
% title('Porownanie wartosci sygnalow w Q_learning')

figure('Position',[ rozm_ekranu(3)/3 rozm_ekranu(4)/2 rozm_ekranu(3)/3 rozm_ekranu(4)/2])
plot(norma);
grid on;
title('Przebieg normy w czasie')


Q_table_rys=Q_table/max(max(Q_table));



% figure('Position',[-rozm_ekranu(3)-50 1 rozm_ekranu(3)+50 rozm_ekranu(4)])
% surf([Q_table(1:ile_st/2,1:ile_ac);Q_table(ile_st/2+2:end,1:ile_ac)])
% grid on;
% title('Q_table po procesie uczenia')

ile_monit=size(get(0, 'MonitorPositions' ));
ile_monit=ile_monit(1);
if ile_monit == 2
    
    close all
    
    QQ=[Q_table(2:ile_st/2-1,:);Q_table(ile_st/2+2:end-1,:)];
    figure('Position',[-rozm_ekranu(3)-50 1 rozm_ekranu(3)+50 rozm_ekranu(4)])
    bar3((QQ-min(min(QQ)))/max(max(QQ-min(min(QQ)))))
    grid on;
    title('Q_table po uczeniu po norm bez stanu doc i skrajnych')
    xlabel('akcje')
    zlabel('Q-value')
    ylabel('stany')
    
    
    
    rozm=size(Q_table);
    zera=zeros(rozm(1),rozm(2));
    for i=1:rozm(1)
        [war,x]=max(Q_table(i,:)) ;
        zera(i,x)=war;
    end
    figure()
    bar3(zera);
    title('Maksymalne wartosci w stanach');
    xlabel('akcje')
    zlabel('Q-value')
    ylabel('stany')
    
    
    figure()
    surf(zera)
    title('Maksymalne wartosci w stanach');
    xlabel('akcje')
    zlabel('Q-value')
    ylabel('stany')
    xlim([1,rozm(2)]);
    ylim([1,rozm(1)]);
    
    %wykres z aproksymacja
    
    rozm=size(zera);
    nr_stanow=[];
    nr_akcji=[];
    for i=1:rozm(1)
        [a,b]=max(zera(i,:));
        nr_stanow=[nr_stanow b];
        nr_akcji=[nr_akcji i];
    end
    
    figure()
    plot(nr_akcji,nr_stanow);
    hold on;
    grid on;
    title("Przebieg wybieranych akcji w stanach");
    ylabel('stany');
    xlabel('akcje');
    ylim([1,nr_stanow(end)]);
    xlim([1,nr_akcji(end)]);
    p = polyfit(nr_akcji,nr_stanow,5);
    v = polyval(p,nr_akcji);
    plot(nr_akcji,v,'r');
    legend('dane','aproksymacja')
    
    
    
    
    
    
    
    
    
    
    %      leg=[];
    %     figure()
    %     wykres=plot(Q_table');
    %     grid on;
    %     for jkl=1:ile_st+1
    %         if jkl~=ile_st/2+1
    %        leg=[leg {['Stan ',num2str(jkl)]}] ;
    %         else
    %           leg=[leg {['Stan docelowy ',num2str(jkl)]}] ;
    %           wykres(jkl).LineWidth=2;
    %         end
    %     end
    %     legend(leg);
    %     xlabel('Nr akcji');
    %     ylabel('Q_value');
    
else
    QQ=[Q_table(2:ile_st/2-1,:);Q_table(ile_st/2+2:end-1,:)];
    
    figure('Position',[1 rozm_ekranu(4)/2 rozm_ekranu(3)/3 rozm_ekranu(4)/2])
    bar3((QQ-min(min(QQ)))/max(max(QQ-min(min(QQ)))))
    grid on;
    title('Q_table po uczeniu po norm bez stanu doc i skrajnych')
    xlabel('akcje')
    zlabel('Q-value')
    ylabel('stany')
    
    
    
    rozm=size(Q_table);
    zera=zeros(rozm(1),rozm(2));
    for i=1:rozm(1)
        [war,x]=max(Q_table(i,:)) ;
        zera(i,x)=war;
    end
    figure()
    bar3(zera);
    title('Maksymalne wartosci w stanach');
    xlabel('akcje')
    zlabel('Q-value')
    ylabel('stany')
    
    %     leg=[];
    %     figure()
    %     wykres=plot(Q_table');
    %     grid on;
    %     for jkl=1:ile_st+1
    %         if jkl~=ile_st/2+1
    %        leg=[leg {['Stan ',num2str(jkl)]}] ;
    %         else
    %           leg=[leg {['Stan docelowy ',num2str(jkl)]}] ;
    %           wykres(jkl).LineWidth=2;
    %         end
    %     end
    %     legend(leg);
    %     xlabel('Nr akcji');
    %     ylabel('Q_value');
    %     figure('Position',[1 1 rozm_ekranu(3)+50 rozm_ekranu(4)])
    %     bar3(Q_table)
    %     grid on;
    %     title('Q_table po procesie uczenia po normalizacji')
    %     xlabel('akcje')
    %     zlabel('Q-value')
    %     ylabel('stany')
    
end

% figure('Position',[1 1 rozm_ekranu(3)+50 rozm_ekranu(4)])
% surf([Q_table_rys(1:ile_st/2,1:ile_ac);Q_table(ile_st/2+2:end,1:ile_ac)])
% grid on;
% title('Q_table po procesie uczenia po normalizacji')

% figure('Position',[1 1 rozm_ekranu(3)+50 rozm_ekranu(4)])
% bar3(Q_table_rys);
% grid on;
% title('Q_table po procesie uczenia po normalizacji')
% xlabel('akcje')
% zlabel('Q-value')
% ylabel('stany')



% surf(Q_table(10:end-1,1:ile_ac-1))
% grid on;
% title('Q_table po procesie uczenia')
% xlabel('akcje')
% zlabel('Q-value')
% ylabel('stany')


figure()
surf(zera)
title('Maksymalne wartosci w stanach');
xlabel('akcje')
zlabel('Q-value')
ylabel('stany')
xlim([1,rozm(2)]);
ylim([1,rozm(1)]);

%wykres z aproksymacja

rozm=size(zera);
nr_stanow=[];
nr_akcji=[];
for i=1:rozm(1)
    [a,b]=max(zera(i,:));
    nr_stanow=[nr_stanow b];
    nr_akcji=[nr_akcji i];
end

figure()
plot(nr_akcji,nr_stanow);
hold on;
grid on;
title("Przebieg wybieranych akcji w stanach");
ylabel('stany');
xlabel('akcje');
ylim([1,nr_stanow(end)]);
xlim([1,nr_akcji(end)]);
p = polyfit(nr_akcji,nr_stanow,5);
v = polyval(p,nr_akcji);
plot(nr_akcji,v,'r');
legend('dane','aproksymacja')