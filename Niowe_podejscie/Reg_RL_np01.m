function [wA]=Reg_RL_np01(Yzad,t0,tc,nis,k,Tob,Tobr,kobr,zak10,zak20,zak30,Yzad_sk,szer_przedz,zakres1,eta,fQname);
% testowanie regulacji na bazie otrzymanego uczenia na podstawie wczytanej
% macierzy
% perwowz�r Reg_RLearning6bbb1(0.5,0,0.1,60,1.94,1.05,1.05,1.94,0.2,0,0,0.2,0.5,5,0.001,'mQ18','wAn9','wAo9',0.3);
% tc [min]
% dzeta- szeroko�� przedzia�u docelowego
% k - wzmocnienie obiektu
% wer- czy pierwszy czy drugi zestaw nastaw
% Top- czas po�nienia w obiekcie
% Topr- czas po�nienia brany do strojenia regulatora
% kobr -wzmocnienie obiektu brane do regulatora
% Yzad_sk- skok warto�ci zadanej
% Reg_RLearning6b(Yzad,t0,tc,nis,k,Tob,Tobr,kobr,zak10,zak20,zak30,Yzad_sk,szer_przedz,zakres1,eta,fQname,fAn,fAo);
% Reg_RLearning1(0.5,  0,0.01,600,1.94,1.05,1.05,1.94,  0.2,    0,    0,    0.2,       0.02,        20,0.001,'mQ9','wAn9','wAo9');
% Reg_RL_np01(0.5,0,0.1,60,1.94,1.05,1.05,1.94,0.2,0,0,0.2,0.05,5,0.001,'mQ16'); 
clc    % czyszczenie ekranu
format long
%%% parametry strojenia Dla inercji I rz�du kpi=Ti/(Tc*kob)
%%% Tc przyjmujemy 0.5*Ti
Tr=Tobr;
kpi=Tr/(0.8*Tr*kobr)
kr_stare=0.002*Tr/(0.4*Tr*kobr)
kr=kpi;  %*tc/Tr
gamma=0.9;   alpha=0.5;         % learning parameter
%%alpha=0.5;
oldQ = 0;   t0pom=t0;
Yzadpocz=Yzad;
%[stany1,zakres_e,zb_Akc,nr_Akc,stan_doc,c_l_przedz] = gen_zakres_stan2(szer_przedz,eta,Tr,tcd,zakres1);
 [zakres_bl,zb_bl,nr_bl_doc,r_zak_bl,M_stan,M_akcji,nr_akcji_doc,l_wiera,l_kola ,przes] = gen_zakres_stan24(szer_przedz,eta,Tr,tc,zakres1);
 %%%rozm_Akc(2)okre�la ilo�� akcji dla danego stanu
% ile_akcji=c_l_przedz;
% Stan_docelowy_nr=stan_doc;

 Wx2=[]; %%% wektor stanu obiektu
 Wu=[]; %%% wektor sterowania

% [R] = gen_R1(c_l_przedz,Stan_docelowy_nr,10,300 );
% %R
% Q=zeros(size(R));      % initialize Q as zero
% 
% q1=ones(size(R))*0;  % initialize previous Q

%count=0;               % counter
dolna_gr=Yzad-szer_przedz; gorna_gr=Yzad+szer_przedz;
iteracja=1;
count=0;
Ti_tc=(Tr+tc)/Tr;
for episode=0:300   
%iteracja=iteracja+1
   % random initial state
   
   X2P=dolna_gr+(gorna_gr-dolna_gr)*rand;
   u=X2P/k; up=u;
   Wx2=[X2P];
   Wu=[u]; Wt=[0];czas=0;

%    okre�lenie b��du i stanu w kt�rym uk�ad znalaz� si� po inicjalizacji
   e=Yzad-X2P;
   [state] = stan_e(zakres_e,e);
   state_p=state;
   i=1; Stan_doc=0;  %%% licznik ile dt uk�ad musi pozosta� w stanie docelowym
                     %%% aby mo�na przyj�� zako�czenie epizodu
while (Stan_doc<550)&& (i<1500)
i=i+1;
% obliczenia obiektu
 %[X2]=ob_stat(k,u,tc,Tob,X2P);
 [X2]=inercja(k,u,tc,Tob,X2P);
   e=Yzad-X2;
  [state] = stan_e(zakres_e,e);
  % select any action from this state

   x=find(R(state,:)>=0);        % find possible action of this state

   if size(x,1)>0,

      aklcja_los=randperm(length(x));   % randomize the possible action
      z_st=zb_Akc(x(aklcja_los(1)));    % select an action 
      nr_a=nr_Akc(x(aklcja_los(1)));    % okre�lenie numeru akcji
         if i==2
             x1p=nr_a;
         end
   end   
      u=up+kr*(z_st);
            if u<0
                u=0;
            end
            if u>1
                u=1;
            end
Wu=[Wu,u];
Wx2=[Wx2,X2];czas=czas+tc;
Wt=[Wt,czas];
up=u;
X2P=X2;
    if state==Stan_docelowy_nr
       Stan_doc=Stan_doc+1;
    else
       Stan_doc=0;
    end
 MaxQ=max(Q,[],2);  % poszukuje maksimum dla kolumn

   x1=nr_a;
   Q(state_p,x1p)=  q1(state_p,x1p) + alpha * (R(state,x1)+ (gamma * MaxQ(x1)) - q1(state_p,x1p)); % new one that I need to implement
   state_p=state;
   x1p=x1;

end

   % break if convergence: small deviation on q for 1000 consecutive

   if sum(sum(abs(q1-Q)))<5 & sum(sum(Q > 0))

      if count>1000,

         episode        % report last episode

         break          % for

      else

         count=count+1; % set counter if deviation of q is small
         q1=Q; 

      end

   else

      q1=Q;

      count=0; % reset counter when deviation of q from previous q is large

   end

end
%%%%%%%%%%%%%%%%%%%%%%%%% sterowanie na podstawie otrzymanych wynik�w
%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%
load (fQname,'Q'); % ko�cowa warto�� macierzy Q
i=0;
czas=0;
   %X2P=Yzad-0.41; %wprowadzone zak��cenie
    X2P=Yzad-0.03; %0.0501;
   ep=Yzad-X2P;
   [state_w] = stan_e(zakres_bl,ep);
   erp=zb_bl(state_w);
   u=X2P/k; up=u;
Wus=[u];
Wx2s=[X2P];
Wts=[0]; Wnr=[];Wns=[];
%%%%%%%%%%% inicjalizacja dla PI
Wupi=[up];
Wx2pi=[X2P];
uppi=up;
upi=up;
X2Ppi=X2P;      
eppi=Yzad-X2P;  
Ti_tcm=(Tr-tc)/Tr;
Wx2erf=[X2P,X2P];
erefp=ep;
%%%%%%%%%%%%%%%%%%
 
while (i<99)
i=i+1;
% obliczenia obiektu
 X2P;
 u;
 [X2]=inercja(k,u,tc,Tob,X2P);
 % trajektoria referencyjna na potrzeby weryfikacji, czy si� dostosowa� do
 % tej trajektorii
 eref=Ti_tcm*erefp;
 X2ref=Yzad-0.0005-eref;
 erefp=eref;
 % obliczenie b��du
   e=Yzad-X2;
 % ograniczenie b��du gdyby przekroczy� ograniczenia  
   if e> szer_przedz
       e=szer_przedz;
   elseif e<-szer_przedz
       e=-szer_przedz;
   end
   % wskazanie wiersza macierzy
   state_w = stan_e(zakres_bl,e)
   er=zb_bl(state_w)

  stan_e1=er*Ti_tc-erp % obliczenie stanu uk�adu
%    l_wiera
%    l_kola
   pom=M_stan(state_w,:);
   %%%%% wskazanie kolumny
   state_k= stan_en1(pom,stan_e1)

  % select any action from this state
%teraz inaczej realizujemu ograniczenia R
   x=find(R(state,:)>=0);        % find possible action of this state

   if size(x,1)>0
% 
%     %%%  aklcja_los=randperm(length(x));   % randomize the possible action
    maxQ=max(Q(state,x));
            if maxQ<=-0.001  %zabezpieczenie gdyby macierz Q by�a niew�a�ciwa
                blad=3
                z_st=0
            else
              nr_a=find(Q(state,x)>=max(Q(state,x))) ; %%% wyb�r max ale spo�r�d dopuszczalnych warto�ci
              nr_ak=x(nr_a(1));
              z_st=zb_Akc(nr_ak)  ;  % select an action Stan_docelowy_nr
             % z_st=zb_Akc(Stan_docelowy_nr)
            end
             if i==2
                 x1p=nr_a(1);
             end
   else
       blad=2
   end   
%%%% okre�lenie akcji
if state_w==nr_bl_doc
    if (state_k>=nr_akcji_doc-1)&&(state_k<=nr_akcji_doc+1)
    z_st=0;
    else
         if er>0
        temp1=eta*Ti_tc;
    else
        temp1=-eta*Ti_tc;
    end
     if erp>0
        temp2=eta;
    else
        temp2=-eta;
    end
    z_st=M_akcji(state_w,state_k)+temp1+temp2;   
    end
else
if (state_k==nr_akcji_doc)
    z_st=0;
else
    if er>0
        temp1=eta*Ti_tc;
    else
        temp1=-eta*Ti_tc;
    end
     if erp>0
        temp2=eta;
    else
        temp2=-eta;
    end
    z_st=M_akcji(state_w,state_k)+temp1+temp2;
end
end
z_st
      u=up+kr*(z_st);
            if u<0
                u=0;
            end
            if u>1
                u=1;
            end
Wus=[Wus,u];
Wx2s=[Wx2s,X2];czas=czas+tc;
Wts=[Wts,czas];
% Wnr=[Wnr,nr_ak];
% Wns=[Wns,state];
 Wx2erf=[Wx2erf,X2ref];
up=u; ep=e; erp=er;
X2P=X2;
 %%%%%%%%%%%%%%%%%%%%%%%%%% regulator PI
 [X2pi]=inercja(k,upi,tc,Tob,X2Ppi);
   epi=Yzad-X2pi;
%    e=Yzad-zmienna_p1;
    if abs(epi)<(eta/2)
        epi=0;
    end
 % [nr_przedz] = stan_e(zakres_e,e); 
 %if d1>-1
   z_stpi=(epi-eppi)+epi*tc/Tr; % przyrost sterowania PI odpowiadaj�cy "akcji" 
   %z_st=e*tc/Tr;
   upi=uppi+kpi*(z_stpi);
   %sume=sume+e;
    %up=kr*tc*sume/Tr;
            if upi<0
                upi=0;
            end
            if upi>1
                upi=1;
            end
Wupi=[Wupi,upi];
Wx2pi=[Wx2pi,X2pi];
uppi=upi;
X2Ppi=X2pi;      
eppi=epi;  
 %end   
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

end
%koncowy_stan=Stan_docelowy_nr
%%%%%%%%%%%%%%%%%%    koniec sterowania
%g=max(max(Q))
%episode
%size(Wx2)
%size(Wu)
%save (fQname,'Q'); % ko�cowa warto�� macierzy Q
% figure
% plot(Wt,Wx2)
% xlabel('min');
% title('wyjscie');
% figure
% plot(Wt,Wu)
% xlabel('min');
% title('sterowanie');
%        figure
%         mesh(Q)
%         title('mQ');
%    [mQn] = stan_mQ1(Q,R);
%           figure
%        hhww2=mesh(mQn)
%       % axis([0 40 0 40 0 1]);
%        xlabel('action number');
%        ylabel('state number');
%        zlabel('Q value');
%         title('matrix Q');
%end  
figure
rozm_d=size(Wts)
l_w_zad=ones(rozm_d)*Yzad;
h_gor=l_w_zad+eta;
h_dol=l_w_zad-eta;
WX2ref1=Wx2erf(1:length(Wx2erf)-1);
size(Wx2pi)
hhww2=plot(Wts,Wx2s,'k-',Wts,Wx2pi,'k:',Wts,WX2ref1,'k--',Wts,h_gor,'k-.',Wts,h_dol,'k-.')
xlabel('min');
set(hhww2(1),'lineWidth',1.0);
set(hhww2(2),'lineWidth',1.5);
set(hhww2(5),'lineWidth',0.5);
set(hhww2(4),'lineWidth',0.5);
axis([0 10 0.44 0.52]);
title('process output');
legend('Q-learning','PI','T_ref');
grid on
%%%%%%%%%%%%%%%%%%%%%%%%%%
% hhww2=plot(Wts,Wx2s,'k-',Wts,Wx2pi,'k:',Wts,WX2ref1,'k--')
% %set(hhww2(1),'lineStyle','-','lineWidth',2.0);
% set(hhww2(1),'lineWidth',1.0);
% set(hhww2(2),'lineWidth',1.5);
%%%%%%%%%%%%%%%%%%%%%%%%%
figure
hhww2=plot(Wts,Wus,'k-',Wts,Wupi,'k:')
xlabel('min');
set(hhww2(1),'lineWidth',1.0);
set(hhww2(2),'lineWidth',1.5);
title('maniputated variable');
legend('Q-learning','PI');
grid on
% figure
% plot(Wnr)
% xlabel('probki');
% title('numery akcji');
% grid on
% figure
% hhww2=plot(Wns,'k-')
% set(hhww2(1),'lineWidth',1.0);
% xlabel('samples');
% title('states numbers');
% grid on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
plot(Wts,Wx2pi)
xlabel('min');
title('wyjscie sterowane PI');
grid on
figure
plot(Wts,Wupi)
xlabel('min');
title('sterowanie PI');
grid on
nr_bl_doc
nr_akcji_doc
%c_l_przedz
M_stan(82,:)
stan_e1
nr=stan_en1(M_stan(82,:),stan_e1)
przes