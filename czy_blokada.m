function [wynik] = czy_blokada(x,y,pozx,pozy,blokady)
wynik=true;

if pozy<1 || pozy>y || pozx<1 || pozx>x
    wynik=false;
else
    for i=1:length(blokady)
        if pozx==blokady(i,2) && pozy==blokady(i,1)
            wynik=false;
        end
    end
end
end

