clear all
close all
clc

for lpo=1:1
    
    %greedy factor
    eps=0;
    %learning rate
    alfa=0.5;
    %discount rate
    gamma=0.8;
    %rozmiar macierzy
    dlugosc=10;
    %0 -automat , 1 reczny
    tryb=0;
    %nagroda
    nagroda=100;
    
    %dokladnosc normy
    dokl_norm=0.1;
    
    %rozmiar pamieci normy
    norm_wek_flag_size=1;
    
    %dokladnosc aproksymacji
    dokladnosc_aproksymacji=0.1;
    
    %mac --> wek
    n=dlugosc^2;
    
    %obliczanie gamma
    wsp_gamma=1;
    gamma=((wsp_gamma/nagroda))^(1/(n-1));
    
    %liczba epok
    max_epoki=1000;
    
    %max liczba iteracji
    max_iter=3000;
    
    m_inicjalizacja
    
    %okre�lony cel
    cely=floor(dlugosc/2);
    celx=floor(dlugosc/2);
    
    
    
    %tworzenie blokad
    for i=1:y-2
        blokady=[blokady; [i 2]; [y-i+1 x-1]];
    end
    %brak blokad
    blokady=[x+10 y+10;x+10 y+10];
    
    blokady_prez=sort(blokady',2);
    fprintf('Q_learning_game %s\nDane wej�ciowe:\n',datestr(datetime,0));
    fprintf('Rozmiar planszy: %dx%d, R = %d \nalfa = %f, gamma = %f, wsp. gamma = %.2f\n',x,y,nagroda,alfa,gamma,wsp_gamma);
    fprintf('Ilo�c epok = %d, max. ilo�� ruch�w = %d, dok�adno�� FOPDT = %f\n',max_epoki,max_iter,dokladnosc_aproksymacji);
    fprintf('Blokady:\n');
    fprintf('y: ');
    fprintf('%d ',blokady_prez(1,:));
    fprintf('\n');
    fprintf('x: ');
    fprintf('%d ',blokady_prez(2,:));
    fprintf('\n');
    
    %% iteracje
    kroczek=0.0005;
    %for epoka=1:(1/kroczek)
    for epoka=1:max_epoki
        eps=eps+kroczek;
        eps=0;
        
        %inicjalizacja zmiennych
        stop=0;
        pozx=randi([1,x],1,1);
        pozy=randi([1,y],1,1);
        while (pozx==celx && pozy==cely) || ...
                ((sum(pozx==blokady(:,2))~=0) && (sum(pozy==blokady(:,1))~=0))
            pozx=randi([1,x],1,1);
            pozy=randi([1,y],1,1);
        end
        
        powtorzenia=0;
        stop_final=0;
        while stop==0
            powtorzenia=powtorzenia+1;
            for i=1:y
                for j=1:x
                    if i==pozy && j==pozx
                        mac(i,j)={'x'};
                    elseif i==cely && j==celx
                        mac(i,j)={'O'};
                    else
                        ss=1;
                        for k=1:length(blokady)
                            if i==blokady(k,1) && j==blokady(k,2)
                                mac(i,j)={'#'};
                                ss=0;
                            end
                        end
                        if ss==1
                            mac(i,j)={' '};
                        end
                    end
                end
            end
            
            if epoka==1 && powtorzenia==1
                fprintf('\nMapa rozgrywki: O-cel, x-pozycja gracza, #-pole zakazane\n')
                disp(mac)
                textprogressbar('Stan uczenia: ',max_epoki);
            end
            textprogressbar(epoka,max_epoki);
            %% logika
            
            %losowanie wsp a
            a = randi([0, 100], [1, 1])/100;
            
            %Szukanie rekordu w Q_table
            for i=2:stany+1
                if Q_table(i,1)==pozy && Q_table(i,2)==pozx
                    wek_ruch=[Q_table(i,3:akcje+2)];
                    wybrany_stan=i;
                    break;
                end
            end
            %exploration/exploitation problem
            if tryb==0
                if eps<=a
                    wybor=randi([1, akcje-1], [1, 1]);
                else
                    [Qsa, wybor]=max(wek_ruch);
                    ile_powt=sum(wek_ruch(:)==wybor);
                    if ile_powt>1
                        los=randi([1, ile_powt], [1, 1]);
                        iter=1;
                        for i=1:akcje
                            if wek_ruch(i) == Qsa && iter == i
                                wybor=i;
                            end
                            iter=iter+1;
                        end
                    end
                end
            else
                m_sterowanie_reczne
            end
            
            old_pozx=pozx;
            old_pozy=pozy;
            
            if pozx==celx && pozy==cely
                wybor=5;
            end
            %ruch gracza
            m_ruch
            
            %% upgrade q-table
            
            %warto�� nagrody
            if celx==old_pozx && cely==old_pozy
                R=nagroda;
                stop=1;
            else
                if kara==0
                    R=0;
                    stop=0;
                else
                    R=nagroda*-10;
                    stop=0;
                end
            end
            maxS=-5000;
            for i=2:stany+1
                if (Q_table(i,1)==pozy+1 && Q_table(i,2)==pozx) || (Q_table(i,1)==pozy-1 && Q_table(i,2)==pozx) ||...
                        (Q_table(i,1)==pozy && Q_table(i,2)==pozx+1) || (Q_table(i,1)==pozy && Q_table(i,2)==pozx-1)...
                        || (Q_table(i,1)==pozy && Q_table(i,2)==pozx)
                    
                    if max(Q_table(i,3:akcje+2))>maxS
                        maxS=max(Q_table(i,3:akcje+2));
                    end
                end
            end
            Q_table(wybrany_stan,wybor+2)=Q_table(wybrany_stan,wybor+2) + ...
                alfa*(R+gamma*maxS-Q_table(wybrany_stan,wybor+2));
            
            if powtorzenia > max_iter
                ilosc_zatrzyman = ilosc_zatrzyman+1;
                break;
            end
        end
        dane_wek=[dane_wek, powtorzenia];
        eps_wek=[eps_wek eps];
        epoka_wek=[epoka_wek epoka];
        
        TT_old=TT;
        for i=1:y
            for j=1:x
                [row, col]=find(Q_table(:,1)==i&Q_table(:,2)==j);
                TT(i,j)=max(Q_table(row,3:akcje+2));
            end
        end
        suma_norm=sum(sum(TT-TT_old));
        norm_wek=[norm_wek suma_norm];
        
        if suma_norm<dokl_norm
            flag1=1;
        else
            flag1=0;
            norm_wek_flag_i=1;
        end
        
        if flag1==1
            norm_wek_flag_i=norm_wek_flag_i+1;
            if norm_wek_flag_i >= norm_wek_flag_size
               norm_wek_flag_i = norm_wek_flag_size; 
               norm_wek_warunek=[norm_wek_warunek 1];
            else
               norm_wek_warunek=[norm_wek_warunek 0];
            end
        else
            norm_wek_warunek=[norm_wek_warunek 0];
        end
        
        q_3d(:,:,i_3d)=TT;
        i_3d=i_3d+1;
    end
    textprogressbar('zakonczono',max_epoki);
    fprintf('\nPrzekroczono %d ruch�w w %d epokach\n',max_iter,ilosc_zatrzyman);
    %aproksymacja �redni� w zale�no�ci od stan�w ustalonych
    aproksymacja=aproksymacja_wykres(q_3d,dokladnosc_aproksymacji);
    
    m_rysowanie_wykresow
    
    m_przyblizenie_inercja
    
    %testetstet
    
end
