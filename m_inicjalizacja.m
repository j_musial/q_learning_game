%rozmiar macierzy kwadratowej (planszy)
x=dlugosc;
y=dlugosc;
ile_pol=x*y;
%inicjalizacja macierzy
mac=[{}];
dane_wek=[];
q_3d=zeros(x,y);
eps_wek=[];
epoka_wek=[];
blokady=[];
norm_wek=[];
norm_wek_warunek=[];
norm_wek_flag=zeros(1, norm_wek_flag_size);
norm_wek_flag_i=1;
TT=zeros(x);
i_3d=2;
ilosc_zatrzyman=0;
%losowanie celu
%przykladowa macierz i numeracja
%| 1 | 2 | 3 |
%| 4 | 5 | 6 |
celx=randi([1,x],1,1);
cely=randi([1,y],1,1);


%rozmiar Q-table
stany=y*x;
akcje=5;
%tworzenie Q-table
Q_table=zeros(stany+1,akcje+2);

%uzupe�nianie stanow i akcji (nag�owki tabeli q)
Q_table(1,1)=NaN;
Q_table(1,2)=NaN;
%numery akcji
for i=1:akcje
    Q_table(1,i+2)=i;
end
%numery stanow x i y
m=0;
for i=1:y
    for j=1:x
        Q_table(j+1+m,1)=i;
        Q_table(j+1+m,2)=j;
    end
    m=y*i;
end

%kszta� Q-table
%stan/akcje
%| NaN | NaN |gora | dol |lewo |prawo| nic |
%|  y1  | x1 |  Q  |  Q  |  Q  |  Q  |  Q  |
%|  y1  | x2 | ... | ... | ... | ... | ... |
%Q_table
% for i=2:stany+1
%     if Q_table(i,1)==cely && Q_table(i,2)==celx
%         break;
%     end
% end
powtorzenia=0;
