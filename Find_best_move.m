function [m,x,y]  = Find_best_move(table)

rozm=size(table);
m=table(1,1);
y=1;
x=1;
yy=[];
xx=[];

for i=1:rozm(1)
    for j=1:rozm(2)
        if table(i,j)>m
           x=j;
           y=i;
           m=table(i,j);
           yy=y;
           xx=x;
        elseif table(i,j)==m
           yy=[yy i];
           xx=[xx j];
        end
    end
end

dl = length(xx);
if dl>1
    r=randi([1,dl],1,1);
    y=yy(r);
    x=xx(r);
end

