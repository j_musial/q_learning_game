function aproksymacja = aproksymacja_wykres(q_3d,dokl)
[x,y,iter]=size(q_3d);
mac=q_3d(:,:,iter);
stany=[0];
powt=0;

%1. stworzenie wektora istniejacych stan�w
for i=1:x
    for j=1:y
        powt=0;
        for k=1:length(stany)
            if mac(i,j)>stany(k)-dokl && mac(i,j)<stany(k)+dokl
                powt=1;
            end
        end
        
        if powt==0
            stany=[stany mac(i,j)];
            
        end
    end
end

%2. rozbiecie maciuerzy na wektor wektor�w dla danej grupy warto�ci
stany(stany > -dokl & stany < dokl) = [];

aproksymacja=[];
for stany_i=1:length(stany)
    wek_wek=[];
    wek_wek_i=1;
    for i=1:x
        for j=1:y
            if mac(i,j)>stany(stany_i)-dokl && mac(i,j)<stany(stany_i)+dokl
                wek_wek(:,:,wek_wek_i)=q_3d(i,j,:);
                wek_wek_i=wek_wek_i+1;
            end
        end
    end
    
    %wyznaczanie wartosci sredniej dla kolejnych iteracji dla wektorow o
    %tym samym stanie ustalonym
    for i=1:length(wek_wek(1,1,:))
        for j=1:length(wek_wek(:,1,1))
            aproksymacja(j,stany_i)=mean(wek_wek(j,:,:));
        end
    end
end

end