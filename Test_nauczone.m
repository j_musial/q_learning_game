%rozmiar macierzy
q_3d=[];
x=10;
y=10;
i_3d=1;
%inicjalizacja macierzy
mac=[{}];
dane=[];
dane_iter=[];
%losowanie celu
%przykladowa macierz i numeracja
%| 1 | 2 | 3 |
%| 4 | 5 | 6 |
ile_pol=x*y;
celx=randi([1,x],1,1);
cely=randi([1,y],1,1);
cely=2;
celx=2;
%cel=1;

%rozmiar Q-table
stany=y*x;
akcje=4;
%tworzenie Q-table
Q_table=zeros(stany+1,akcje+2);

%uzupe�nianie stanow i akcji (nag�owki tabeli q)
Q_table(1,1)=NaN;
Q_table(1,2)=NaN;
%numery akcji
for i=1:akcje
    Q_table(1,i+2)=i;
end
%numery stanow x i y
m=0;
for i=1:y
    for j=1:x
        Q_table(j+1+m,1)=i;
        Q_table(j+1+m,2)=j;
    end
    m=y*i;
end

%kszta� Q-table
%stan/akcje
%| NaN | NaN |gora | dol |lewo |prawo| nic |
%|  y1  | x1 |  Q  |  Q  |  Q  |  Q  |  Q  |
%|  y1  | x2 | ... | ... | ... | ... | ... |
%Q_table
for i=2:stany+1
    if Q_table(i,1)==cely && Q_table(i,2)==celx
        % Q_table(i,3:6)=[100 100 100 100];
        break;
    end
end
%Q_table

%greedy factor
eps=1;
%learning rate
alfa=0.1;
%discount rate
gamma=0.99;

powtorzenia=0;
eps_wek=[];

%% iteracje
%inicjalizacja zmiennych
stop=0;
pozx=randi([1,x],1,1);
pozy=randi([1,y],1,1);
while pozx==celx && pozy==cely
    pozx=randi([1,x],1,1);
    pozy=randi([1,y],1,1);
end

powtorzenia=0;
stop_final=0;
while stop==0
    powtorzenia=powtorzenia+1;
    for i=1:y
        for j=1:x
            %             if i==cely && j==celx
            %                 mac(i,j)={100};
            if i==pozy && j==pozx
                mac(i,j)={'x'};
            elseif i==cely && j==celx
                mac(i,j)={'O'};
            else
                mac(i,j)={' '};
            end
        end
    end
    %% logika
    
    %losowanie wsp a
    a = randi([0, 100], [1, 1])/100;
    
    %Szukanie rekordu w Q_table
    for i=2:stany+1
        if Q_table(i,1)==pozy && Q_table(i,2)==pozx
            wek_ruch=[Q_table(i,3:akcje+2)];
            wybrany_stan=i;
            break;
        end
    end
    %exploration/exploitation problem
    if eps<=a
        wybor=randi([1, akcje], [1, 1]);
        %disp('losowo');
    else
        %disp('tabela');
        [Qsa, wybor]=max(wek_ruch);
        ile_powt=sum(wek_ruch(:)==wybor);
        if ile_powt>1
            los=randi([1, ile_powt], [1, 1]);
            iter=1;
            for i=1:akcje
                if wek_ruch(i) == Qsa && iter == i
                    wybor=i;
                end
                iter=iter+1;
            end
        end
    end
    
    %ruch gracza
    m_ruch
    
    %% upgrade q-table
    
    %             %warto�� nagrody
    %             if celx==pozx && cely==pozy
    %                 R=100;
    %                 stop=1;
    %             else
    %                 R=0;
    %                 stop=0;
    %             end
    %             % wek_maxS=[];
    %             %i_maxS=1;
    %             maxS=-5000;
    %             for i=2:stany+1
    %                 if (Q_table(i,1)==pozy+1 && Q_table(i,2)==pozx) || (Q_table(i,1)==pozy-1 && Q_table(i,2)==pozx) ||...
    %                         (Q_table(i,1)==pozy && Q_table(i,2)==pozx+1) || (Q_table(i,1)==pozy && Q_table(i,2)==pozx-1)
    %                     % wek_maxS(i_maxS)=i;
    %                     if max(Q_table(i,3:akcje+2))>maxS
    %                         maxS=max(Q_table(i,3:akcje+2));
    %                     end
    %                     %maxS=max(Q_table(i,3:akcje+2));
    %                 end
    %             end
    %
    %             Q_table(wybrany_stan,wybor+2)=Q_table(wybrany_stan,wybor+2) + ...
    %                 alfa*(R+gamma*maxS-Q_table(wybrany_stan,wybor+2));
    %
    % Q_table(wybrany_stan,wybor+2)=(1-alfa)*Q_table(wybrany_stan,wybor+2) + ...
    %     alfa*(R+gamma*maxS);
    
    % Q_table
    mac
    pause(0.1);
    if powtorzenia > 3000
        disp('zatrzymano');
        break;
    end
end
%         dane=[dane, powtorzenia];
%         eps_wek=[eps_wek eps];
%         for i=1:y
%             for j=1:x
%                 [row, col]=find(Q_table(:,1)==i&Q_table(:,2)==j);
%                 TT(i,j)=max(Q_table(row,3:akcje+2));
%             end
%         end
%         for i=1:y
%             for j=1:x
%                 if TT(i,j)==0
%                    TT(i,j)=max(max(TT))+0.05*max(max(TT));
%                 end
%             end
%         end
%         q_3d(:,:,i_3d)=TT;
%         i_3d=i_3d+1;
%         %disp('\n');
%         % disp(cell2mat(compose('%10.0f',TT)))
%         %         pause(1);
%     figure(2)
%     plot(eps_wek,dane);
%     hold on;
%     grid on;
%     xlabel('eps');
%     ylabel('ilosc rochow');
%     fprintf('\n');
%     %mac
%     fprintf('\n');
%     disp(cell2mat(compose('%10.0f',Q_table)))
%     fprintf('\n');
% %     for i=1:y
% %         for j=1:x
% %             [row, col]=find(Q_table(:,1)==i&Q_table(:,2)==j);
% %             TT(i,j)=max(Q_table(row,3:akcje+2));
% %         end
% %     end
%
%     disp(cell2mat(compose('%10.0f',TT)))
%     figure(1)
%     wsk=1-0.15*lpo;
%     s=surf(TT,'FaceAlpha',1);
%     %s=mesh(TT);
%     %s.EdgeColor = 'none';
%     %s.LineStyle = ':';
%     hold on;
%
%     figure(3)
%     for i=1:3
%         for j=1:3
%             %wykres()=q_3d(i,j,:);
%             plot(squeeze(q_3d(i,j,:)));
%             hold on;
%         end
%     end
