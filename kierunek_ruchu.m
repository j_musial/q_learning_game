%�rodek
if pozy>2 && pozy<stany+1 && pozx>3 && pozx<akcje+2
    %ruch_wek = [gora dol lewo prawo]
    ruch_wek=[Q_table(pozy-1,pozx) Q_table(pozy+1,pozx) Q_table(pozy,pozx-1) Q_table(pozy,pozx+1)];
    if eps<=a
        wybor=randi([1, 4], [1, 1]);
    else
        [Qsa, wybor]=max(ruch_wek);
    end
end

%gorna krawedz
if pozy==2 && pozy<stany+1 && pozx>3 && pozx<akcje+2
    %ruch_wek = [brak dol lewo prawo]
    ruch_wek=[-inf Q_table(pozy+1,pozx) Q_table(pozy,pozx-1) Q_table(pozy,pozx+1)];
    if eps<=a
        wybor=randi([2, 4], [1, 1]);
    else
        [Qsa, wybor]=max(ruch_wek);
    end
end

%dolna krawedz
if pozy>2 && pozy==stany+1 && pozx>3 && pozx<akcje+2
    %ruch_wek = [gora brak lewo prawo]
    ruch_wek=[Q_table(pozy-1,pozx) -inf Q_table(pozy,pozx-1) Q_table(pozy,pozx+1)];
    if eps<=a
        wybor=2;
        while wybor==2
            wybor=randi([1, 4], [1, 1]);
        end
    else
        [Qsa, wybor]=max(ruch_wek);
    end
end

%lewa krawedz
if pozy>2 && pozy<stany+1 && pozx==3 && pozx<akcje+2
    %ruch_wek = [gora dol brak prawo]
    ruch_wek=[Q_table(pozy-1,pozx) Q_table(pozy+1,pozx) -inf Q_table(pozy,pozx+1)];
    if eps<=a
        wybor=3;
        while wybor==3
            wybor=randi([1, 4], [1, 1]);
        end
    else
        [Qsa, wybor]=max(ruch_wek);
    end
end

%prawa krawedz
if pozy>2 && pozy<stany+1 && pozx>3 && pozx==akcje+2
    %ruch_wek = [gora dol lewo brak]
    ruch_wek=[Q_table(pozy-1,pozx) Q_table(pozy+1,pozx) Q_table(pozy,pozx-1) -inf];
    if eps<=a
        wybor=4;
        while wybor==4
            wybor=randi([1, 4], [1, 1]);
        end
    else
        [Qsa, wybor]=max(ruch_wek);
    end
end

%lewy gorny rog
if pozy==2 && pozy<stany+1 && pozx==3 && pozx<akcje+2
    %ruch_wek = [brak dol brak prawo]
    ruch_wek=[-inf Q_table(pozy+1,pozx) -inf Q_table(pozy,pozx+1)];
    if eps<=a
        wybor=1;
        while wybor==1 || wybor==3
            wybor=randi([1, 4], [1, 1]);
        end
    else
        [Qsa, wybor]=max(ruch_wek);
    end
end

%prawy gorny rog
if pozy==2 && pozy<stany+1 && pozx>3 && pozx==akcje+2
    %ruch_wek = [brak dol lewo brak]
    ruch_wek=[-inf Q_table(pozy+1,pozx) Q_table(pozy,pozx-1) -inf];
    if eps<=a
        wybor=randi([2, 3], [1, 1]);
    else
        [Qsa, wybor]=max(ruch_wek);
    end
end

%dolny lewy rog
if pozy>2 && pozy==stany+1 && pozx==3 && pozx<akcje+2
    %ruch_wek = [gora brak brak prawo]
    ruch_wek=[Q_table(pozy-1,pozx) -inf -inf Q_table(pozy,pozx+1)];
    if eps<=a
        wybor=2;
        while wybor==2 || wybor==3
            wybor=randi([1, 4], [1, 1]);
        end
    else
        [Qsa, wybor]=max(ruch_wek);
    end
end

%dolny prawy rog
if pozy>2 && pozy==stany+1 && pozx>3 && pozx==akcje+2
    %ruch_wek = [gora dol lewo prawo]
    ruch_wek=[Q_table(pozy-1,pozx) Q_table(pozy+1,pozx) Q_table(pozy,pozx-1) Q_table(pozy,pozx+1)];
    if eps<=a
        wybor=randi([1, 4], [1, 1]);
    else
        [Qsa, wybor]=max(ruch_wek);
    end
end